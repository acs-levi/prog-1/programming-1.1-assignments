package be.kdg.prog1.m8.a01interfaces;

public abstract class Shape implements Printable {

    private int x;
    private int y;

    public Shape() {

    }

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public abstract double getArea();
    public abstract double getPerimeter();
}
