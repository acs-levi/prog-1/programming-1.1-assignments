package be.kdg.prog1.m3;

import java.util.Scanner;

public class A08_Divisible {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int firstDivider, secondDivider;
        boolean keepRunning = true;

        System.out.println("We'll print all numbers below 1000 that are divisible between 2 numbers of your choosing.");

        do {

            System.out.print("\nEnter the first divider (end the program with 0): ");
            firstDivider = sc.nextInt();

            if (firstDivider == 0) {
                keepRunning = false;
            }
            else {
                System.out.print("Enter the second divider: ");
                secondDivider = sc.nextInt();

                if (firstDivider < 0 || secondDivider < 0) {
                    System.out.println("Please enter positive numbers");
                }
                else if (secondDivider == 0) {
                    System.out.println("Can't divide by zero!");
                }
                else {
                    for (int i = 1, outputCounter = 1; i < 1000; i++) {
                        if (i % firstDivider == 0 && i % secondDivider == 0) {
                            System.out.print(i + " " + (outputCounter++ % 10 == 0 ? "\n" : ""));
                        }
                    }
                    System.out.println();
                }
            }
        } while(keepRunning);

        sc.close();
    }
}
