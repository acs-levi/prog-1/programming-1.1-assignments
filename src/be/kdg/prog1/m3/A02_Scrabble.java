package be.kdg.prog1.m3;

import java.util.Scanner;

public class A02_Scrabble {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        char input;
        char[] letters = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        int[] letterValues = {1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5, 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4, 4, 8, 4, 10};

        System.out.print("Enter an uppercase letter from the English alphabet: ");
        input = sc.nextLine().charAt(0);

        for (int i = 0; i < letters.length; i++) {
            if (letters[i] == input) {
                System.out.println("The Scrabble value of your letter is " + letterValues[i]);
                break;
            }

            if (i == letters.length - 1) {
                System.out.println("The character you entered is not an uppercase letter.");
            }
        }

        sc.close();
    }
}