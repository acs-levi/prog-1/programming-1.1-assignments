package be.kdg.prog1.m3;

import java.util.Scanner;

public class A04_Multiples {

    public static void main(String[] args) {

        final int MAX = 100;
        int input = 0;
        Scanner sc = new Scanner(System.in);

        System.out.print("Which number would you like to see the multiples of? ");
        input = sc.nextInt();

        for (int i = 1; input * i <= MAX; i++) {
            System.out.println(input * i);
        }

        sc.close();
    }

}