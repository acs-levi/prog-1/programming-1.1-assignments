package be.kdg.prog1.m3;

import java.util.Scanner;

public class A09_Interest {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        double startingCapital;
        double interestRate;
        int numberOfYears;

        System.out.print("Enter the starting capital in €: ");
        startingCapital = sc.nextDouble();

        System.out.print("Enter the interest rate: ");
        interestRate = sc.nextDouble();

        System.out.print("Enter the number of years: ");
        numberOfYears = sc.nextInt();

        System.out.println("The capital will amount to €"
                + (int)compoundInterest(startingCapital, interestRate, numberOfYears));

        //Used Rule of 72 to calculate how long will it take to double the starting capital
        System.out.println("It takes " + (int)Math.ceil(72/interestRate) + " years to double the money.");

        sc.close();
    }

    static double compoundInterest(double startingCapital, double interestRate, int numberOfYears) {
        return startingCapital * Math.pow((1 + interestRate / 100), numberOfYears);
    }
}
