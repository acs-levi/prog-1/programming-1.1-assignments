package be.kdg.prog1.m3;

//Can be solved with recursion very nicely. Need to return long to avoid int overflow

public class A05_Factorial {
    public static void main(String[] args) {

        for (int i = 1; i <= 20 ; i++) {
            System.out.println(i + "! = " + findFactorial(i));
        }
    }

    static long findFactorial(int n) {
        if (n == 1)
            return 1;
        return n * findFactorial(n - 1);
    }
}