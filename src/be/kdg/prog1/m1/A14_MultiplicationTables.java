package be.kdg.prog1.m1;

import java.util.Scanner;

public class A14_MultiplicationTables {

    public static void main(String[] args) {

        int num;
        boolean correctAnswer = true;
        Scanner sc = new Scanner(System.in);

        System.out.print("Which multiplication table would you like to see? ");
        num = sc.nextInt();

        for (int i = 1; i < 11; i++) {
            System.out.println(i + " x " + num + " = " + (i * num));
        }

        System.out.print("Which multiplication table would you like to practice? ");
        num = sc.nextInt();

        for (int i = 1; i < 11 ; i++) {
            do {
                System.out.print(i + " x " + num + " = ");
                if (sc.nextInt() == (i * num)) {
                    System.out.println("Correct!");
                    correctAnswer = true;
                }
                else {
                    System.out.println("Wrong!");
                    correctAnswer = false;
                }
            } while(!correctAnswer);
        }

        sc.close();
    }
}