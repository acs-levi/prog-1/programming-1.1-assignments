package be.kdg.prog1.m1;

import java.util.Scanner;

public class A11_Sequence {
    public static void main(String[] args) {

        int amount, startingValue, increment;
        Scanner sc = new Scanner(System.in);

        System.out.println("How many numbers do you want to print?");
        amount = sc.nextInt();

        System.out.println("What is the starting value?");
        startingValue = sc.nextInt();

        System.out.println("What is the increment?");
        increment = sc.nextInt();

        for (int i = 0; i < amount; i++) {
            System.out.println(startingValue);
            startingValue += increment;
        }

        sc.close();
    }
}

