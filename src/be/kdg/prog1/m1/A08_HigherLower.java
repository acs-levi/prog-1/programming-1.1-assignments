package be.kdg.prog1.m1;

import java.util.Scanner;

public class A08_HigherLower {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int secret = 42;
        int counter = 0;
        int guess = 0;

        while(guess != secret) {
            System.out.print("Enter a number: ");
            guess = sc.nextInt();
            counter++;

            if (guess == secret) {
                System.out.println("Congratulations, you used " + counter + " guesses to get the right number.");
            }
            else if (guess > secret) {
                System.out.println("Lower!");
            }
            else {
                System.out.println("Higher!");
            }
        }

    }
}
