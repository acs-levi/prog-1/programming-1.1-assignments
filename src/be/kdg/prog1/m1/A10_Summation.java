package be.kdg.prog1.m1;

import java.util.Scanner;

public class A10_Summation {

    public static void main(String[] args) {

        int input = -1;
        int sum = 0;
        int terms = 0;
        boolean newCalc;
        char newCalcAnswer;
        Scanner sc = new Scanner(System.in);

        do {
            while (input != 0) {
                System.out.println("Enter a number (to stop enter 0):");
                input = sc.nextInt();
                if (input !=0) {
                    terms++;
                }
                sum += input;
            }

            System.out.println("Sum: " + sum + " Terms: " + terms);

            System.out.println("New calculation? (Press 'y' to proceed, anything else to quit)");
            newCalcAnswer = sc.next().charAt(0);

            if (newCalcAnswer == 'y') {
                newCalc = true;
                input = -1;
                sum = 0;
                terms = 0;
            }
            else {
                System.out.println("Program terminated.");
                newCalc = false;
            }
        } while(newCalc);

        sc.close();
    }
}