package be.kdg.prog1.m7.a09shape;

public class Rectangle extends Shape {

    protected int height;
    protected int width;

    public Rectangle() {

    }

    public Rectangle(int x, int y) {
        super(x, y);
    }

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public double getArea() {
        return width * height;
    }

    public double getPerimeter() {
        return 2 * (height + width);
    }

}