package be.kdg.prog1.m7.a12shape3D;

public class Sphere extends Shape3D {

    protected double radius = 1.0;

    public Sphere() {

    }

    public Sphere(String color, double radius) {
        super(color);
        this.radius = radius;
    }

    public double surface() {
        return 4 * Math.pow(radius, 2) * Math.PI;
    }

    public double volume() {
        return (4 / 3) * Math.pow(radius, 3) * Math.PI;
    }
}
