package be.kdg.prog1.m7.a03product.electronics;

import be.kdg.prog1.m7.a03product.Product;

public class Camera extends Product {

    private int pixels;

    public Camera (String code, String description, double price, int pixels) {
        super(code, description, price);
        this.pixels = pixels;
    }

    public int getPixels() {
        return pixels;
    }

    @Override
    public String toString() {
        return "Camera{" +
                "pixels=" + getPixels() +
                ", code='" + getCode() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", price=" + getPrice() +
                '}';
    }
}
