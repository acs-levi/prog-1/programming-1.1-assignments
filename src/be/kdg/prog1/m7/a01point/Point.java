package be.kdg.prog1.m7.a01point;

import be.kdg.prog1.m7.a09shape.Rectangle;

import java.util.Objects;

public class Point {
    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Point() {

    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }


    @Override
    public String toString() {
        return String.format("x: %d y: %d", x, y);
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (!(obj instanceof Point))
            return false;

        Point o = (Point) obj;
        return x == o.x && y == o.y;
    }

    @Override
    public int hashCode() {
        return x ^ y;
    }
}