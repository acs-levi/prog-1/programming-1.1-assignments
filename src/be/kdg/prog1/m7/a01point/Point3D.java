package be.kdg.prog1.m7.a01point;

public class Point3D extends Point {

    private int z;

    public void setZ(int z) {
        this.z = z;
    }

    public int getZ() {
        return z;
    }

    public Point3D() {

    }

    public Point3D(int x, int y, int z) {
        this.setX(x);
        this.setY(y);
        this.z = z;
    }


    @Override
    public String toString() {
        return super.toString() + " z: " + z;
    }


    @Override
    public boolean equals(Object obj) {

        if (this == obj)
            return true;
        if (!(obj instanceof Point))
            return false;

        Point3D o = (Point3D) obj;
        return getX() == o.getX() && getY() == o.getY() && z == o.z;
    }

    @Override
    public int hashCode() {
        return super.hashCode() ^ z;
    }

}
