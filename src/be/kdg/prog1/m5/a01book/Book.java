package be.kdg.prog1.m5.a01book;

public class Book {

    private String author;
    private String title;
    private int numberOfPages;
    private boolean onLoan;

    public Book() {
        this("unknown", "unknown", 0);
    }

    public Book(String author, String title, int numberOfPages) {
        this.author = author;
        this.title = title;
        this.numberOfPages = numberOfPages;
    }

    public String getAuthor() {
        return author;
    }
    public String getTitle() {
        return title;
    }
    public int getNumberOfPages() {
        return numberOfPages;
    }
    public boolean isOnLoan() {
        return onLoan;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    public void setTitle(String title)  {
        this.title = title;
    }
    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }
    public void setOnLoan(boolean onLoan) {
        this.onLoan = onLoan;
    }

    public String toString() {
        return String.format("Book %s (%d pages), written by %s is %s.",
                title.toUpperCase(), numberOfPages, author.toUpperCase(), (onLoan ? "on loan" : "available"));
    }
}
