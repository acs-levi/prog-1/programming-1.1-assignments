package be.kdg.prog1.m5.a07circle;

public class Circle {
    public int radius;
    public String colour = "black";

    public Circle(int radius, String colour) {
        this.radius = radius;
        this.colour = colour;
    }
    public Circle(int radius) {
        this.radius = radius;
    }

    public double circumference() {
        return 2 * Math.PI * radius;
    }
    public double surface() {
        return Math.PI * Math.pow(radius, 2);
    }
    public double deltaCircumference(Circle other) {
        return Math.abs(this.circumference() - other.circumference());
    }
    public double deltaSurface(Circle other) { //?
        return this.circumference() - other.circumference();
    }

    public String toString() {
        return String.format("Colour: %s\nRadius: %d\nCircumference: %.2f\nSurface: %.2f\n", colour, radius, circumference(), surface());
    }

}
