package be.kdg.prog1.m5.a02bankaccount;

public class BankAccount {

    private String holder;
    private String iban;
    private double balance;

    public BankAccount(String holder, String iban, double balance) {
        this.holder = holder;
        this.iban = iban;
        this.balance = balance;
    }

    //Constructor chaining
    public BankAccount(String holder, String iban) {
        this(holder, iban, 0);
    }

    public String getHolder() {
        return holder;
    }

    public String getIban() {
        return iban;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    public boolean withdraw(double amount) {
        if (this.balance < amount)
            return false;
        this.balance -= amount;
        return true;
    }

    public String toString() {
        return String.format("The account %s of %s has a balance of €%.2f", iban, holder, balance);
    }
}