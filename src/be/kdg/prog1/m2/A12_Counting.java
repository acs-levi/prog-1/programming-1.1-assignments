package be.kdg.prog1.m2;

public class A12_Counting {
    public static void main(String[] args) {

        final int MAX = 10;
        int i = 0;
        int j = 11;

        while(i != MAX) {
            System.out.println(++i + " - " + --j);
        }
    }
}
