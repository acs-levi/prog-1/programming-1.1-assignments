package be.kdg.prog1.m2;

import java.util.Scanner;

public class A14_Encoding {

    static final char A_ASCII = 65;
    static final char Z_ASCII = 90;

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String messageToEncrypt = "";
        String decryptedMessage = "";
        int displacement;

        System.out.print("Enter a message: ");
        messageToEncrypt = sc.nextLine();
        
        System.out.print("Displacement to be used: ");
        displacement = sc.nextInt() % 26;

        messageToEncrypt = encryptCaesarCipher(messageToEncrypt, displacement);

        if (messageToEncrypt != null) {
            System.out.println("Encoded message: " + messageToEncrypt);
        }
        else {
            System.out.println("The string you entered contains one or more non-alphabetic characters.");
        }

        decryptedMessage = decryptCaesarCipher(messageToEncrypt, displacement);

        if (decryptedMessage != null) {
            System.out.println("Decoded message: " + decryptedMessage);
        }
        else {
            System.out.println("Unable to decode message.");
        }

        sc.close();
    }


    static String encryptCaesarCipher(String input, int displacement) {

        StringBuilder sb = new StringBuilder();
        input = input.toUpperCase();
        char[] inputCharArray = input.toCharArray();
        int newCharValue;

        for (int i = 0; i < inputCharArray.length; i++) {

            if (inputCharArray[i] != ' ' && ((int)inputCharArray[i] < A_ASCII || (int)inputCharArray[i] > Z_ASCII)) {
                return null;
            }
            else {
                if(inputCharArray[i] != ' ') {
                    newCharValue = (int)inputCharArray[i] + displacement;

                    if (newCharValue > Z_ASCII) {
                        newCharValue = A_ASCII + (newCharValue - Z_ASCII - 1);
                    }

                    inputCharArray[i] = (char)newCharValue;
                }
            }
        }

        sb.append(inputCharArray);
        return sb.toString();
    }

    static String decryptCaesarCipher(String input, int displacement) {

        StringBuilder sb = new StringBuilder();
        char[] inputCharArray = input.toCharArray();
        int newCharValue;

        for (int i = 0; i < inputCharArray.length; i++) {

            if (inputCharArray[i] != ' ' && ((int)inputCharArray[i] < A_ASCII || (int)inputCharArray[i] > Z_ASCII)) {
                return null;
            }
            else {
                if(inputCharArray[i] != ' ') {
                    newCharValue = (int)inputCharArray[i] - displacement;

                    if (newCharValue < A_ASCII) {
                        newCharValue = Z_ASCII - (A_ASCII - newCharValue - 1);
                    }
                    inputCharArray[i] = (char)newCharValue;
                }
            }
        }

        sb.append(inputCharArray);
        return sb.toString();
    }
}