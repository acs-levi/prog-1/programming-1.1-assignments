package be.kdg.prog1.m2;

import java.util.Scanner;

public class A10_Digits_v1 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int result = 0;
        int firstInput;
        boolean keepRunning = true;

        System.out.println("Enter four positive integers (0..9).");

        while(keepRunning) {
            System.out.print("The first digit: ");

            firstInput = sc.nextInt();
            if (firstInput != -1) {
                result += (1000 * firstInput);

                System.out.print("The second digit: ");
                result += (100 * sc.nextInt());

                System.out.print("The third digit: ");
                result += (10 * sc.nextInt());

                System.out.print("The fourth digit: ");
                result += sc.nextInt();

                System.out.println("The number is " + result);
            }
            else {
                keepRunning = false;
            }
        }

        sc.close();
    }
}
