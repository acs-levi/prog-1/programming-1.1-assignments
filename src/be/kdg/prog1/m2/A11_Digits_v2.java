package be.kdg.prog1.m2;

import java.util.Scanner;

public class A11_Digits_v2 {
    public static void main(String[] args) {

        final int MINIMUM = 1000;
        final int MAXIMUM = 9999;
        int input;
        int digitsSum;
        boolean keepRunning = true;
        Scanner sc = new Scanner(System.in);

        while(keepRunning) {
            System.out.print("Enter a 4-digit whole number (1000...9999): ");

            input = sc.nextInt();
            digitsSum = 0;


            if (input == -1) {
                keepRunning = false;
            }
            else {

                //For the "hard" version: just remove the if-else
                if (input >= MINIMUM && input <= MAXIMUM) {

                    while(input > 0) {
                        digitsSum += input % 10;
                        input /= 10;
                    }

                    System.out.println("The sum of the digits of this number is " + digitsSum);
                }
                else {
                    System.out.println("The number is either too small or too big. Please enter a new number.");
                }
            }
        }
        sc.close();
    }
}
