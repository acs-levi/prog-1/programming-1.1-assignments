package be.kdg.prog1.m2;

import java.util.Scanner;

public class A13_FuelConsumption {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int previousMileage;
        int drivenMileage;
        double litersRefilled;

        System.out.print("Enter the previous mileage: ");
        previousMileage = sc.nextInt();

        System.out.print("Enter the current mileage: ");
        drivenMileage = sc.nextInt() - previousMileage;

        System.out.print("Enter the amount of liters refilled: ");
        litersRefilled = sc.nextDouble();

        System.out.print("Consumption for " + drivenMileage + "km driven: ");
        System.out.format("%.2f", (1 / ((double)drivenMileage / litersRefilled)) * 100);
        System.out.println(" liters/100km");

        sc.close();
    }
}