package be.kdg.prog1.m2;

import java.util.Scanner;

public class A08_ASCIIvalues {
    public static void main(String[] args) {

        char[] arr;
        String input;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a string: ");
        input = sc.nextLine();
        arr = input.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + " has an ASCII value of " + (int)arr[i]);
        }

        sc.close();
    }
}
