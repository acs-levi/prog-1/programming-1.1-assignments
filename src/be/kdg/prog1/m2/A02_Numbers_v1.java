package be.kdg.prog1.m2;

public class A02_Numbers_v1 {

    public static void main(String[] args) {

        /*
        //int a = 2_000_000_000;
        //int b = 2_000_000_000;
        long a = 2_000_000_000;
        long b = 2_000_000_000;

        System.out.println(a + b);

        long c = 10000;
        long d = 10000;
        int result = (int)(c + d);
        System.out.println(result);
        */

        int first = 8;
        int second = 5;

        System.out.println(
                        "Sum: " + (first + second) + "\n" +
                        "Difference: " + (first - second) + "\n" +
                        "Product: " + (first * second) + "\n" +
                        "Quotient: " + ((double)first / second) + "\n" +
                        "Remainder: " + (first % second)
        );

        int result;

        result = ++first;
        System.out.println(result + " " + first);

        result = (first++);
        System.out.println(result + " " + first);

        /*
        result = --second;
        System.out.println(result);

        result = second--;
        System.out.println(result);
        */
    }
}