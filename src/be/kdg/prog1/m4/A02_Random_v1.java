package be.kdg.prog1.m4;

import javax.swing.*;
import java.util.Random;

public class A02_Random_v1 {
    public static void main(String[] args) {

        Random r = new Random();

        for (int i = 0; i < 6; i++)
            System.out.print(r.nextInt(6) + 1 + " ");

        System.out.println();

        for (int j = 0; j < 4; j++)
            System.out.print(r.nextBoolean() + " ");

        System.out.println();

        for (int k = 0; k < 3; k++)
            System.out.print(r.nextDouble() + " ");

        System.out.println();

        for (int l = 0; l < 10; l++)
            System.out.print(r.nextInt(101) + 900 + " ");

        System.out.println();

        for (int m = 0; m < 10; m++)
            System.out.print(r.nextInt(50) * 2 + " ");

        System.out.println();

        for (int n = 0; n < 10; n++) {
            int rand = r.nextInt(100);

            if (rand % 3 == 0)
                System.out.print(rand + " ");
            else
                n--;
        }

    }
}
