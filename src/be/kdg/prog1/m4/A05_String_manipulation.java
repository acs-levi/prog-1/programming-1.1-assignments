package be.kdg.prog1.m4;

import java.util.Scanner;

public class A05_String_manipulation {
    public static void main(String[] args) {

        String input;
        Scanner sc = new Scanner(System.in);

        System.out.print("Please enter a sentence: ");
        input = sc.nextLine();

        System.out.println(input.toUpperCase());
        System.out.println(input.toLowerCase());
        System.out.println(input.replace("a", "o"));
        System.out.println(input.length());
        System.out.println(input.charAt(0));
        System.out.println(input.charAt(input.length() - 1));
        System.out.println(input.length() - input.replace("e", "").length());
    }
}