package be.kdg.prog1.m6;

public class A09_Array_of_characters {
    public static void main(String[] args) {

        String word = "JavaScript";
        char[] arr = word.toCharArray();

        for(char ch : arr)
            System.out.printf("%c ", ch);
    }
}